# AsAg

*AsAg* (**As**signations **Ag**gregation) determines the best taxonomic assignation using [*Kraken 2*](https://github.com/DerrickWood/kraken2) outputs from various databases.

## Table of Contents

- [Description](#description)
- [Dependencies](#dependencies)
- [Configuration](#configuration)
- [Usage](#usage)

## Description

*AsAg* runs *Kraken 2* on your input files once for each database. Then it aggregates the different assignations for each sequence to choose the best one and generate a new *Kraken 2* output.

## Dependencies

*AsAg* uses [*Kraken 2*](https://github.com/DerrickWood/kraken2) to make the assignations, so it must be installed.

The following modules are also required:

- *yaml*
- *pandas*
- *tqdm*

You can install these modules with *pip*:

```
pip install pyyaml pandas tqdm
```

## Configuration

### config.yml

The `config.yml` file contains the main configurations. It contains the following values:

- `kraken_dbs`: The list of all the *Kraken 2* databases you want to use.
- `ref_db`: The reference database. The final output files will be written using this database's taxonomic IDs and names.
- `save_intermediate_kraken_outputs`: `True` or `False`, specifies whether the *Kraken 2* outputs generated with each database should be conserved after the aggregation or not.
- `save_intermediate_kraken_reports`: `True` or `False`, specifies whether the *Kraken 2* reports generated with each database should be conserved after the aggregation or not.

### Maps

Each *Kraken 2* database uses a different taxonomy. One of them will be used as a reference (`ref_db` in `config.yml`). For all the other, a `maps/tax_map_{db_name}.map` file is needed. This file contains the mapping in the format `tax_id	ref_tax_id` (tab separated, one per line). `tax_id` is the taxonomic ID in the database and `ref_tax_id` is the corresponding taxonomic ID in the reference database. If no file is provided, it will be considered that both databases use the same taxonomy.

The map file can be automatically generated with `map_db.py`. To do so, make sure that the reference database is set in `config.yml` and run:

```
map_db.py --db /path/to/database
```

This will search for common taxon names and map them together. If you want to set different mapping rules, your can implement the `apply_conditions(ref_tax, tax_name, level)` function in a Python file in the `map_conditions` folder and specify it with:

```
map_db.py --db /path/to/database --conditions silva
```

A [*Silva*](https://www.arb-silva.de) and [*Dairydb*](https://github.com/marcomeola/DAIRYdb) files are provided along with the *naive* default one.

## Usage

Once your configuration is ready, you can use it like this:

```
usage: asag.py --input <INPUT> --output <OUTPUT> --report <REPORT>

optional arguments:
  -h, --help             Show this help message and exit
  -i, --input INPUT      Input to be given to Kraken 2
  -o, --output OUTPUT    The Kraken 2 output file to generate
  -r, --report REPORT    The Kraken 2 report to generate
```
If you get a `Too many open files` error, run the following command and try again:

```
ulimit -n 4096
```
