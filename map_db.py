#!/bin/python3

import os
import yaml
import importlib
import argparse
import pandas as pd
from tqdm import tqdm
tqdm.pandas()

CONFIG_FILE = "config.yml"

parser = argparse.ArgumentParser(description='Creates the map file to be used with AsAg')
parser.add_argument('-d', '--db', required=True, help='The Kraken 2 database to map with the reference dayabase')
parser.add_argument('-c', '--conditions', help='The conditions file to use (default: naive)')
args = parser.parse_args()

# Read configuration files
python_files_directory = os.path.dirname(os.path.abspath(__file__))

print("Open files")
with open(os.path.join(python_files_directory, CONFIG_FILE), "r") as file:
    config = yaml.safe_load(file)

ref_tax_file = f"{config['ref_db']}/taxonomy/names.dmp"
tax_file = f"{args.db}/taxonomy/names.dmp"

ref_nodes_file = f"{config['ref_db']}/taxonomy/nodes.dmp"
nodes_file = f"{args.db}/taxonomy/nodes.dmp"

ref_tax = pd.read_csv(ref_tax_file, sep="\t\|\t", header=None, usecols=range(2), names=['id', 'name'], engine='python')
tax = pd.read_csv(tax_file, sep="\t\|\t", header=None, usecols=range(2), names=['id', 'name'], engine='python')

ref_nodes = pd.read_csv(ref_nodes_file, sep="\t\|\t", header=None, usecols=range(3), names=['id', 'parent', 'level'], engine='python')
nodes = pd.read_csv(nodes_file, sep="\t\|\t", header=None, usecols=range(3), names=['id', 'parent', 'level'], engine='python')

tax = pd.merge(tax, nodes, on='id')
ref_tax = pd.merge(ref_tax, ref_nodes, on='id')

COL_ID = 0
COL_NAME = 1
COL_PARENT = 2
COL_LEVEL = 3

if args.conditions is None:
    conditions_file = "default"
else:
    conditions_file = args.conditions

apply_conditions = importlib.import_module(f"map_conditions.{conditions_file}").apply_conditions

level_dfs = {}

# Retrieves the taxonomic ID in the reference database from a DF row
def get_ref_id(row):
    tax_name = row[COL_NAME]
    level = row[COL_LEVEL]

    if level in level_dfs:
        level_df = level_dfs[level]
    else:
        level_df = ref_tax[ref_tax['level']==level]
        level_dfs[level] = level_df

    lines = apply_conditions(level_df, tax_name, level)

    if len(lines)==0:
        #print(f"No match found for {row}")
        return None
    if len(lines)>1:
        pass
        #print(f"{tax_name}: ({len(lines)} possibilities):") #TODO choose the right one instead of taking the first one.
        #print(lines)

    return int(lines['id'].values[0])

print("Map taxa")
ref_ids = tax.progress_apply(get_ref_id, axis=1)
print(f"{ref_ids.count()}/{len(tax)} taxa mapped ({round(100*ref_ids.count()/len(tax), 1)}%)")

os.makedirs(os.path.join(python_files_directory, "maps"), exist_ok=True)
tax['ref_id'] = ref_ids.astype('Int64')
tax[['id', 'ref_id']].to_csv(os.path.join(python_files_directory, "maps", f"tax_map_{os.path.basename(args.db)}.map"), sep='\t', index=False)
