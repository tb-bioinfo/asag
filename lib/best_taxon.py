import os
import yaml
from lib.tax_tree import KTree

tree = None
weights = {}

def get_tree():
    global tree
    if tree is None:
        CONFIG_FILE = "config.yml"
        python_files_directory = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(python_files_directory, "..", CONFIG_FILE), "r") as file:
            config = yaml.safe_load(file)
        tree = KTree(config['ref_db'])
    return tree

def get_weight(db, assignation):
    return 1

# Get the most probable taxon from a set of assignations
def get_best_taxon(assignations):
    tree = get_tree()
    scores = {}
    for db, assignation in assignations.items():
        if assignation!=0:
            score = 0
            node = tree.get_node(assignation)

            if node is None:
                pass
                # print(f"Node not found: {assignation}")
            else:
                while not node is None:
                    if node.tax_id in assignations.values():
                        score += get_weight(db, assignation)
                    node = node.parent
                
                score = scores.get(assignation, 0) + score
                scores[assignation] = score
    
    if len(scores)==0:
        return 0
        
    return max(scores, key=scores.get)
