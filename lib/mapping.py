import os
import pandas as pd

maps = {}

# Open mapping file and store it in RAM
def open_db(db):
    python_files_directory = os.path.dirname(os.path.abspath(__file__))
    try:
        data = pd.read_csv(os.path.join("maps", f"tax_map_{db}.map"), delimiter='\t', header=0, names=['tax_id', 'mapped'],
                   dtype={'tax_id': int, 'mapped': float}, na_values='', keep_default_na=False)
    except FileNotFoundError:
        maps[db] = None
        return None
    data['mapped'].fillna(0, inplace=True)
    data['mapped'] = data['mapped'].astype(int)
    data.set_index('tax_id', inplace=True)
    maps[db] = data
    return data

# Get the mapping from RAM or from file if not loaded yet
def get_map(db):
    return maps.get(db, open_db(db))

# Map a tax ID from given database to reference database
def map_taxon(tax_id, from_db, ref_db=None):
    if from_db==ref_db:
        return tax_id

    tax_id_map = get_map(from_db)

    if tax_id_map is None:
        return tax_id
    
    try:
        value = tax_id_map.loc[tax_id, 'mapped']
        return int(value)
    except KeyError:
        return 0
