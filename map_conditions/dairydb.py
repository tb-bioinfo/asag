def apply_conditions(ref_tax, tax_name, level):
    # Dairydb: "Bacillus Bacillus_thermophilus" -> "Bacillus thermophilus"
    tax_name = tax_name.split()[-1].replace('_', ' ')

    if level=="phylum":
        level_condition = (ref_tax['name']+" Phylum"==tax_name)
    elif level=="class":
        level_condition = (ref_tax['name']+" Class"==tax_name)
    elif level=="order":
        level_condition = (ref_tax['name']+" Order"==tax_name)
    elif level=="family":
        level_condition = (ref_tax['name']+" Family"==tax_name)
    elif level=="genus":
        level_condition = (ref_tax['name']+" Genus"==tax_name)
    elif level=="species":
        level_condition = (ref_tax['name']+" Species"==tax_name) | (ref_tax['name']==tax_name+" sp.")
    else:
        level_condition = False

    result = ref_tax[
        (ref_tax['name']==tax_name) |
        level_condition
    ]

    if len(result)>0:
        return result

    return ref_tax[
        ref_tax['name']==tax_name.replace(" Species", " sp.") |
        (ref_tax['name']=="Candidatus "+tax_name)
    ]
