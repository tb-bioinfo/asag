def apply_conditions(ref_tax, tax_name, level):
    if tax_name=="Escherichia-Shigella":
        return ref_tax[
            ref_tax['name']=="Escherichia"
        ]
    return ref_tax[
        ref_tax['name']==tax_name
    ]
