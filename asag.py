#!/bin/python3

import yaml
import argparse
import subprocess
import shlex
import os
import glob
from tqdm import tqdm
from lib.mapping import map_taxon
from lib.best_taxon import get_best_taxon

parser=argparse.ArgumentParser(description="""Program to run Kraken 2 on different databases and choose the best result for each sequence.""")

parser.add_argument('-i', '--input', help='The input files for Kraken 2.', nargs='+', type=str)
parser.add_argument('-o', '--output', help='The Kraken 2 output file.', type=str)
parser.add_argument('-r', '--report', help='The Kraken 2 report file to generate.', type=str)

args = parser.parse_args()
print(os.path.commonprefix(args.input))

def usage():
    parser.print_help()

# Read configuration file
CONFIG_FILE = "config.yml"
python_files_directory = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(python_files_directory, CONFIG_FILE), "r") as file:
    config = yaml.safe_load(file)
    print(config['kraken_dbs'])

# Run Kraken with each database
kraken_results = {os.path.basename(kraken_db): subprocess.check_output(shlex.split(f"kraken2 --db {kraken_db} {f'--report {os.path.splitext(args.report)[0]}-{os.path.basename(kraken_db)}{os.path.splitext(args.report)[1]}' if config['save_intermediate_kraken_reports'] else ''} {' '.join(args.input)}")).decode('utf8').split('\n') for kraken_db in config['kraken_dbs']}

if config['save_intermediate_kraken_outputs']:
    for db, output in kraken_results.items():
        with open(f"{os.path.splitext(args.output)[0]}-{db}.kraken2", "w") as kraken_file:
            kraken_file.write('\n'.join(output))

results = {}
# For each sequence in the results
for values in zip(*kraken_results.values()):
    assignations = {}
    # Retrieve the assignation made by each database for the current sequence
    for db, value in zip(kraken_results.keys(), values):
        if len(value)>0:
            line = value.split('\t')
            seq_id = line[1]
            taxon = line[2]

            assignations[db] = int(taxon)
    if len(assignations)>0:
        results[seq_id] = assignations

with open(args.output, "w") as result_file:
    ref_db = os.path.basename(config['ref_db'])
    buffer = []
    # Determine the best assignation for each sequence
    for seq_id, assignations in tqdm(results.items()):
        # Map on the reference database
        assignations = {db: map_taxon(assignation, db, ref_db=ref_db) for db, assignation in assignations.items()}
        # Get the best assignation
        best_taxon = get_best_taxon(assignations)
        # Write to output file
        result_file.write(f"{'U' if best_taxon==0 else 'C'}\t{seq_id}\t{best_taxon}\t\n")

# Generate report
os.system(f"{os.path.join(python_files_directory, 'lib', 'kraken2-report')} {os.path.join(config['ref_db'], 'taxo.k2d')} {args.output} {args.report}")
